webpackJsonp([0],{

/***/ 113:
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = 113;

/***/ }),

/***/ 155:
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = 155;

/***/ }),

/***/ 199:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ListsPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(33);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__todos_todos__ = __webpack_require__(200);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__shared_lists_service__ = __webpack_require__(204);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var ListsPage = (function () {
    function ListsPage(navCtrl, navParams, alertCtrl, listsService, loaderCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.alertCtrl = alertCtrl;
        this.listsService = listsService;
        this.loaderCtrl = loaderCtrl;
        this.selectedList = null;
    }
    ListsPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad ListsPage');
    };
    ListsPage.prototype.goToList = function (list) {
        this.clearSelectedList();
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_2__todos_todos__["a" /* TodosPage */], { list: list });
    };
    ListsPage.prototype.addNewList = function (name) {
        var _this = this;
        var loader = this.loaderCtrl.create();
        loader.present();
        this.listsService.addList(name)
            .subscribe(function (list) {
            _this.goToList(list);
            loader.dismiss();
        }, function (error) {
            loader.dismiss();
        });
    };
    ListsPage.prototype.showAddList = function () {
        var _this = this;
        var addListAlert = this.alertCtrl.create({
            title: 'New List',
            message: 'Give a name to the new list',
            inputs: [
                {
                    name: 'name',
                    placeholder: 'Name'
                }
            ],
            buttons: [
                {
                    text: 'Cancel',
                    handler: function (data) { }
                },
                {
                    text: 'Add',
                    handler: function (data) {
                        var navTransition = addListAlert.dismiss();
                        navTransition.then(function () { _this.addNewList(data.name); });
                        return false;
                    }
                }
            ]
        });
        addListAlert.present();
    };
    ListsPage.prototype.clearSelectedList = function () {
        this.selectedList = null;
    };
    ListsPage.prototype.selectList = function (list) {
        console.log('yes, i press and hold the tap');
        if (this.selectedList == list) {
            this.clearSelectedList();
        }
        else {
            this.selectedList = list;
        }
    };
    ListsPage.prototype.removeSelectedList = function () {
        this.listsService.removeList(this.selectedList);
        this.selectedList = null;
    };
    ListsPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-lists',template:/*ion-inline-start:"/Users/jombuena/Documents/ionic3Course/ioniclists/src/pages/lists/lists.html"*/`<ion-header>\n\n  <ion-navbar color="primary">\n    <ion-buttons end *ngIf="selectedList != null">\n      <button ion-button (click)="removeSelectedList()">\n        <ion-icon name="trash"></ion-icon>\n      </button>\n    </ion-buttons>\n    <ion-title>My Lists</ion-title>\n  </ion-navbar>\n  \n</ion-header>\n\n<ion-content padding>\n    <ion-list>\n      <ion-item *ngFor="let list of listsService.lists" (press)="selectList(list)" (tap)="goToList(list)"\n        [ngClass]="{\'selected-list\': selectedList === list}">\n        {{list.name}}\n      </ion-item>\n    </ion-list>\n  </ion-content>\n\n<ion-fab bottom right>\n  <button ion-fab (click)="showAddList()">\n    <ion-icon name="add"></ion-icon>\n  </button>\n</ion-fab>\n`/*ion-inline-end:"/Users/jombuena/Documents/ionic3Course/ioniclists/src/pages/lists/lists.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_3__shared_lists_service__["a" /* ListsServiceProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* LoadingController */]])
    ], ListsPage);
    return ListsPage;
}());

//# sourceMappingURL=lists.js.map

/***/ }),

/***/ 200:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TodosPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(33);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__add_task_modal_add_task_modal__ = __webpack_require__(201);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__shared_todo_service__ = __webpack_require__(203);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var TodosPage = (function () {
    function TodosPage(navCtrl, modalCtrl, todoService, platform, navParams) {
        this.navCtrl = navCtrl;
        this.modalCtrl = modalCtrl;
        this.todoService = todoService;
        this.platform = platform;
        this.navParams = navParams;
        this.toggleTodoTimeout = null;
        this.list = this.navParams.get('list');
        this.todoService.loadFromList(this.list.id);
    }
    TodosPage.prototype.ionViewDidLoad = function () { };
    TodosPage.prototype.ionViewWillUnload = function () {
        this.todoService.saveLocally(this.list.id);
    };
    TodosPage.prototype.setTodoStyles = function (item) {
        var styles = {
            'text-decoration': item.isDone ? 'line-through' : 'none',
            'font-weight': item.isImportant ? '600' : 'normal'
        };
        return styles;
    };
    TodosPage.prototype.toggleTodo = function (todo) {
        var _this = this;
        if (this.toggleTodoTimeout) {
            return;
        }
        this.toggleTodoTimeout = setTimeout(function () {
            _this.todoService.toggleToDo(todo);
            _this.toggleTodoTimeout = null;
        }, this.platform.is('ios') ? 0 : 350);
    };
    TodosPage.prototype.removeTodo = function (todo) {
        this.todoService.removeTodo(todo);
    };
    TodosPage.prototype.showAddTodo = function () {
        var _this = this;
        var modal = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_2__add_task_modal_add_task_modal__["a" /* AddTaskModalPage */]);
        modal.present();
        modal.onDidDismiss(function (data) {
            if (data) {
                _this.todoService.addTodo(data);
            }
        });
    };
    TodosPage.prototype.showEditTodo = function (todo) {
        var _this = this;
        var modal = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_2__add_task_modal_add_task_modal__["a" /* AddTaskModalPage */], { todo: todo });
        modal.present();
        modal.onDidDismiss(function (data) {
            if (data) {
                _this.todoService.updateTodo(todo, data);
            }
        });
    };
    TodosPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-todos',template:/*ion-inline-start:"/Users/jombuena/Documents/ionic3Course/ioniclists/src/pages/todos/todos.html"*/`\n<ion-header>\n\n  <ion-navbar color="favorite">\n    <ion-title>To Do List</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content padding>\n<h3>{{list.name}}</h3>\n  <ion-list>\n\n    <ion-item class="no-todos" *ngIf="!todoService.todos || todoService.todos.length == 0">Add some tasks to your list</ion-item>\n    <ion-item-sliding *ngFor="let todo of todoService.todos | prioritizedTodosPipe">\n     \n      <ion-item [ngStyle]="setTodoStyles(todo)">\n        <ion-label>{{todo.description}}</ion-label>\n        <ion-checkbox [checked]="todo.isDone" (click)="todoService.toggleToDo(todo)"></ion-checkbox>\n      </ion-item>\n    \n      <ion-item-options side="right">\n        <button ion-button color="dark" (click)="showEditTodo(todo)">\n          <ion-icon name="create"></ion-icon>\n          Edit</button>\n        <button ion-button color="danger" (click)="removeTodo(todo)">\n          <ion-icon name="remove-circle"></ion-icon>\n          Remove</button>\n      </ion-item-options>\n\n    </ion-item-sliding>\n    <ion-item-divider *ngIf="(todoService.todos | doneTodosPipe).length > 0">Done tasks</ion-item-divider>\n    <ion-item *ngFor="let todo of todoService.todos | doneTodosPipe" [ngStyle]="setTodoStyles(todo)">\n        <ion-label color="light">{{todo.description}}</ion-label>\n        <ion-checkbox color="dark" [checked]="todo.isDone" (click)="todoService.toggleToDo(todo)"></ion-checkbox>\n    </ion-item>\n  </ion-list>\n</ion-content>\n\n<ion-fab bottom right class="add-task">\n    <button ion-fab (click)=showAddTodo() >\n      <ion-icon name="add"></ion-icon>\n    </button>\n  </ion-fab>`/*ion-inline-end:"/Users/jombuena/Documents/ionic3Course/ioniclists/src/pages/todos/todos.html"*/,
            providers: [__WEBPACK_IMPORTED_MODULE_3__shared_todo_service__["a" /* TodoServiceProvider */]]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* ModalController */],
            __WEBPACK_IMPORTED_MODULE_3__shared_todo_service__["a" /* TodoServiceProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* Platform */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavParams */]])
    ], TodosPage);
    return TodosPage;
}());

//# sourceMappingURL=todos.js.map

/***/ }),

/***/ 201:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AddTaskModalPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(33);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__shared_todo_model__ = __webpack_require__(202);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



/**
 * Generated class for the AddTaskModalPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var AddTaskModalPage = (function () {
    function AddTaskModalPage(viewCtrl, navParams) {
        this.viewCtrl = viewCtrl;
        this.navParams = navParams;
        this.model = new __WEBPACK_IMPORTED_MODULE_2__shared_todo_model__["a" /* TodoModel */]('');
        this.title = "Add new Task";
        this.buttonText = "ADD";
        if (this.navParams.get('todo')) {
            this.model = __WEBPACK_IMPORTED_MODULE_2__shared_todo_model__["a" /* TodoModel */].clone(this.navParams.get('todo'));
            this.title = "Edit task";
            this.buttonText = "Save changes";
        }
    }
    AddTaskModalPage.prototype.submit = function () {
        this.viewCtrl.dismiss(this.model);
    };
    AddTaskModalPage.prototype.dismiss = function () {
        this.viewCtrl.dismiss();
    };
    AddTaskModalPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-add-task-modal',template:/*ion-inline-start:"/Users/jombuena/Documents/ionic3Course/ioniclists/src/pages/add-task-modal/add-task-modal.html"*/`<!--\n  Generated template for the AddTaskModalPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n<ion-toolbar color="primary">\n  <ion-title>{{title}}</ion-title>\n  <ion-buttons start>\n    <button ion-button (click)="dismiss()">\n      <span color="secondary" showWhen="ios">Cancel</span>\n      <ion-icon name="md-close" showWhen="android, windows"></ion-icon>\n  </button>\n  </ion-buttons>\n</ion-toolbar>\n\n</ion-header>\n\n<ion-content>\n\n  <ion-list no-lines>\n    <form class="container" (ngSubmit)="submit()">\n\n      <ion-item>\n        <ion-input type="text" placeholder="Task Description" clearInput\n        [(ngModel)]="model.description" name="description" required\n        #descriptionState="ngModel"></ion-input>\n      </ion-item>\n\n      <ion-item ion-text color="warning" [hidden]="descriptionState.valid || descriptionState.untouched">\n        Description is required\n      </ion-item>\n\n      <ion-item>\n        <ion-label>Its important</ion-label>\n          <ion-checkbox color="dark" [(ngModel)]="model.isImportant"\n          name="isImportant"></ion-checkbox>\n      </ion-item>\n\n      <div class="submit-button">\n        <button ion-button block type="submit"\n        [disabled]="!descriptionState.valid">{{buttonText}}</button>\n      </div>\n\n    </form>\n  </ion-list>\n</ion-content>\n`/*ion-inline-end:"/Users/jombuena/Documents/ionic3Course/ioniclists/src/pages/add-task-modal/add-task-modal.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* ViewController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavParams */]])
    ], AddTaskModalPage);
    return AddTaskModalPage;
}());

//# sourceMappingURL=add-task-modal.js.map

/***/ }),

/***/ 202:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TodoModel; });
var TodoModel = (function () {
    function TodoModel(description, isImportant, isDone) {
        if (isImportant === void 0) { isImportant = false; }
        if (isDone === void 0) { isDone = false; }
        this.description = description;
        this.isImportant = isImportant;
        this.isDone = isDone;
    }
    TodoModel.clone = function (todo) {
        return new TodoModel(todo.description, todo.isImportant, todo.isDone);
    };
    return TodoModel;
}());

//# sourceMappingURL=todo-model.js.map

/***/ }),

/***/ 203:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TodoServiceProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__ionic_storage__ = __webpack_require__(102);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__todo_model__ = __webpack_require__(202);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_http__ = __webpack_require__(99);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var TodoServiceProvider = (function () {
    function TodoServiceProvider(http, local) {
        this.http = http;
        this.local = local;
        this.todos = [];
    }
    TodoServiceProvider.prototype.loadFromList = function (id) {
        this.getFromLocal(id);
    };
    TodoServiceProvider.prototype.getFromLocal = function (id) {
        var _this = this;
        return this.local.ready().then(function () {
            return _this.local.get("list/" + id).then(function (data) {
                if (!data) {
                    _this.todos = [];
                    return;
                }
                var localTodos = [];
                for (var _i = 0, data_1 = data; _i < data_1.length; _i++) {
                    var todo = data_1[_i];
                    localTodos.push(new __WEBPACK_IMPORTED_MODULE_2__todo_model__["a" /* TodoModel */](todo.description, todo.isImportant, todo.isDone));
                }
                _this.todos = localTodos;
            });
        });
    };
    TodoServiceProvider.prototype.toggleToDo = function (todo) {
        var isDone = !todo.isDone;
        var todoIndex = this.todos.indexOf(todo);
        var updatedTodo = new __WEBPACK_IMPORTED_MODULE_2__todo_model__["a" /* TodoModel */](todo.description, todo.isImportant, isDone);
        this.todos = this.todos.slice(0, todoIndex).concat([
            updatedTodo
        ], this.todos.slice(todoIndex + 1));
    };
    TodoServiceProvider.prototype.saveLocally = function (id) {
        var _this = this;
        this.local.ready().then(function () {
            _this.local.set("list/" + id, _this.todos);
        });
    };
    TodoServiceProvider.prototype.addTodo = function (todo) {
        this.todos = this.todos.concat([todo]);
    };
    TodoServiceProvider.prototype.removeTodo = function (todo) {
        var index = this.todos.indexOf(todo);
        this.todos = this.todos.slice(0, index).concat(this.todos.slice(index + 1));
    };
    TodoServiceProvider.prototype.updateTodo = function (originalTodo, modifiedTodo) {
        var index = this.todos.indexOf(originalTodo);
        this.todos = this.todos.slice(0, index).concat([
            modifiedTodo
        ], this.todos.slice(index + 1));
    };
    TodoServiceProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_3__angular_http__["a" /* Http */], __WEBPACK_IMPORTED_MODULE_1__ionic_storage__["b" /* Storage */]])
    ], TodoServiceProvider);
    return TodoServiceProvider;
}());

//# sourceMappingURL=todo-service.js.map

/***/ }),

/***/ 204:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ListsServiceProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__(99);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map__ = __webpack_require__(282);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_share__ = __webpack_require__(283);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_share___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_share__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_storage__ = __webpack_require__(102);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__list_model__ = __webpack_require__(284);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__app_settings__ = __webpack_require__(285);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var ListsServiceProvider = (function () {
    function ListsServiceProvider(http, local) {
        this.http = http;
        this.local = local;
        this.lists = [];
        this.getLists();
    }
    ListsServiceProvider.prototype.getLists = function () {
        var _this = this;
        this.getFromLocal()
            .then(function () { _this.getFromServer(); }, function () { _this.getFromServer(); });
    };
    ListsServiceProvider.prototype.addList = function (name) {
        var _this = this;
        var observable = this.postNewListToServer(name);
        observable.subscribe(function (list) {
            _this.lists = _this.lists.concat([list]);
            _this.saveLocally();
        }, function (error) { return console.log("Rerro trying to post a new list to the server"); });
        return observable;
    };
    ListsServiceProvider.prototype.getFromLocal = function () {
        var _this = this;
        return this.local.ready().then(function () {
            return _this.local.get('lists').then(function (data) {
                var localLists = [];
                if (data) {
                    for (var _i = 0, data_1 = data; _i < data_1.length; _i++) {
                        var list = data_1[_i];
                        localLists.push(new __WEBPACK_IMPORTED_MODULE_5__list_model__["a" /* ListModel */](list.name, list.id));
                    }
                }
                _this.lists = localLists;
            });
        });
    };
    ListsServiceProvider.prototype.getFromServer = function () {
        var _this = this;
        this.http.get(__WEBPACK_IMPORTED_MODULE_6__app_settings__["a" /* AppSettings */].API_ENDPOINT + "/lists")
            .map(function (response) { return response.json(); })
            .map(function (lists) {
            return lists.map(function (item) { return __WEBPACK_IMPORTED_MODULE_5__list_model__["a" /* ListModel */].fromJson(item); });
        })
            .subscribe(function (result) {
            _this.lists = result;
            _this.saveLocally();
        }, function (error) {
            console.log('Error loading lists from server');
        });
    };
    ListsServiceProvider.prototype.postNewListToServer = function (name) {
        var observable = this.http.post(__WEBPACK_IMPORTED_MODULE_6__app_settings__["a" /* AppSettings */].API_ENDPOINT + "/lists", { name: name })
            .share()
            .map(function (response) { return response.json(); })
            .map(function (list) { return __WEBPACK_IMPORTED_MODULE_5__list_model__["a" /* ListModel */].fromJson(list); });
        observable.subscribe(function () { }, function () { });
        return observable;
    };
    ListsServiceProvider.prototype.deleteListFromServer = function (id) {
        var observable = this.http.delete(__WEBPACK_IMPORTED_MODULE_6__app_settings__["a" /* AppSettings */].API_ENDPOINT + "/lists/" + id)
            .map(function (response) { return response.json(); }).share();
        return observable;
    };
    ListsServiceProvider.prototype.saveLocally = function () {
        var _this = this;
        this.local.ready().then(function () {
            _this.local.set('lists', _this.lists);
        });
    };
    ListsServiceProvider.prototype.removeList = function (list) {
        var _this = this;
        this.deleteListFromServer(list.id).subscribe(function () {
            var index = _this.lists.indexOf(list);
            _this.lists = _this.lists.slice(0, index).concat(_this.lists.slice(index + 1));
            _this.saveLocally();
            console.log('a list was removed, list:', "" + list.name);
        }, function (error) {
            console.log("an error occurred while trying to remove list: " + list.name);
        });
    };
    ListsServiceProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Http */], __WEBPACK_IMPORTED_MODULE_4__ionic_storage__["b" /* Storage */]])
    ], ListsServiceProvider);
    return ListsServiceProvider;
}());

//# sourceMappingURL=lists-service.js.map

/***/ }),

/***/ 205:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__ = __webpack_require__(206);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__app_module__ = __webpack_require__(228);


Object(__WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__["a" /* platformBrowserDynamic */])().bootstrapModule(__WEBPACK_IMPORTED_MODULE_1__app_module__["a" /* AppModule */]);
//# sourceMappingURL=main.js.map

/***/ }),

/***/ 228:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__ = __webpack_require__(26);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(33);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_http__ = __webpack_require__(99);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_native_status_bar__ = __webpack_require__(195);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ionic_native_splash_screen__ = __webpack_require__(197);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__ionic_native_screen_orientation__ = __webpack_require__(198);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__ionic_storage__ = __webpack_require__(102);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__app_component__ = __webpack_require__(281);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__pages_todos_todos__ = __webpack_require__(200);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__pages_lists_lists__ = __webpack_require__(199);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__pages_add_task_modal_add_task_modal__ = __webpack_require__(201);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__shared_todo_service__ = __webpack_require__(203);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__shared_lists_service__ = __webpack_require__(204);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__pipes_prioritized_todos__ = __webpack_require__(286);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15__pipes_done_todos__ = __webpack_require__(287);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
















var AppModule = (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_8__app_component__["a" /* MyApp */],
                __WEBPACK_IMPORTED_MODULE_9__pages_todos_todos__["a" /* TodosPage */],
                __WEBPACK_IMPORTED_MODULE_10__pages_lists_lists__["a" /* ListsPage */],
                __WEBPACK_IMPORTED_MODULE_11__pages_add_task_modal_add_task_modal__["a" /* AddTaskModalPage */],
                __WEBPACK_IMPORTED_MODULE_14__pipes_prioritized_todos__["a" /* PrioritizedTodosPipe */],
                __WEBPACK_IMPORTED_MODULE_15__pipes_done_todos__["a" /* DoneTodosPipe */]
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__["a" /* BrowserModule */],
                __WEBPACK_IMPORTED_MODULE_3__angular_http__["b" /* HttpModule */],
                __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["d" /* IonicModule */].forRoot(__WEBPACK_IMPORTED_MODULE_8__app_component__["a" /* MyApp */], {
                    mode: 'ios',
                    scrollPadding: 'adjustNothing'
                }, {
                    links: []
                }),
                __WEBPACK_IMPORTED_MODULE_7__ionic_storage__["a" /* IonicStorageModule */].forRoot()
            ],
            bootstrap: [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["b" /* IonicApp */]],
            entryComponents: [
                __WEBPACK_IMPORTED_MODULE_8__app_component__["a" /* MyApp */],
                __WEBPACK_IMPORTED_MODULE_9__pages_todos_todos__["a" /* TodosPage */],
                __WEBPACK_IMPORTED_MODULE_10__pages_lists_lists__["a" /* ListsPage */],
                __WEBPACK_IMPORTED_MODULE_11__pages_add_task_modal_add_task_modal__["a" /* AddTaskModalPage */],
            ],
            providers: [
                { provide: __WEBPACK_IMPORTED_MODULE_1__angular_core__["u" /* ErrorHandler */], useClass: __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["c" /* IonicErrorHandler */] },
                __WEBPACK_IMPORTED_MODULE_12__shared_todo_service__["a" /* TodoServiceProvider */],
                __WEBPACK_IMPORTED_MODULE_13__shared_lists_service__["a" /* ListsServiceProvider */],
                __WEBPACK_IMPORTED_MODULE_4__ionic_native_status_bar__["a" /* StatusBar */],
                __WEBPACK_IMPORTED_MODULE_5__ionic_native_splash_screen__["a" /* SplashScreen */],
                __WEBPACK_IMPORTED_MODULE_6__ionic_native_screen_orientation__["a" /* ScreenOrientation */]
            ]
        })
    ], AppModule);
    return AppModule;
}());

//# sourceMappingURL=app.module.js.map

/***/ }),

/***/ 281:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MyApp; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(33);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__ = __webpack_require__(195);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__ = __webpack_require__(197);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_native_screen_orientation__ = __webpack_require__(198);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__pages_lists_lists__ = __webpack_require__(199);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var MyApp = (function () {
    function MyApp(platform, statusBar, splashScreen, screenOrientation) {
        this.rootPage = __WEBPACK_IMPORTED_MODULE_5__pages_lists_lists__["a" /* ListsPage */];
        platform.ready().then(function () {
            // Okay, so the platform is ready and our plugins are available.
            // Here you can do any higher level native things you might need.
            statusBar.styleDefault();
            splashScreen.hide();
            if (screenOrientation.type == 'ios' || screenOrientation.type == 'android') {
                screenOrientation.lock(screenOrientation.ORIENTATIONS.PORTRAIT);
            }
            else {
                screenOrientation.unlock();
            }
        });
    }
    MyApp = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({template:/*ion-inline-start:"/Users/jombuena/Documents/ionic3Course/ioniclists/src/app/app.html"*/`<ion-nav [root]="rootPage">\n</ion-nav>\n`/*ion-inline-end:"/Users/jombuena/Documents/ionic3Course/ioniclists/src/app/app.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* Platform */],
            __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__["a" /* StatusBar */],
            __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__["a" /* SplashScreen */],
            __WEBPACK_IMPORTED_MODULE_4__ionic_native_screen_orientation__["a" /* ScreenOrientation */]])
    ], MyApp);
    return MyApp;
}());

//# sourceMappingURL=app.component.js.map

/***/ }),

/***/ 284:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ListModel; });
var ListModel = (function () {
    function ListModel(name, id) {
        this.name = name;
        this.id = id;
    }
    ListModel.fromJson = function (data) {
        if (!data.name || !data.id)
            throw (new Error('Invalid argument: argument structure do not match with model'));
        return new ListModel(data.name, data.id);
    };
    return ListModel;
}());

//# sourceMappingURL=list-model.js.map

/***/ }),

/***/ 285:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppSettings; });
var AppSettings = (function () {
    function AppSettings() {
    }
    Object.defineProperty(AppSettings, "API_ENDPOINT", {
        get: function () {
            return 'http://localhost:3000';
        },
        enumerable: true,
        configurable: true
    });
    return AppSettings;
}());

//# sourceMappingURL=app-settings.js.map

/***/ }),

/***/ 286:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PrioritizedTodosPipe; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

/**
 * Generated class for the PrioritizedTodosPipe pipe.
 *
 * See https://angular.io/api/core/Pipe for more info on Angular Pipes.
 */
var PrioritizedTodosPipe = (function () {
    function PrioritizedTodosPipe() {
    }
    PrioritizedTodosPipe.prototype.transform = function (todos) {
        return todos.filter(function (todo) { return !todo.isDone; }).sort(function (a, b) { return (b.isImportant && !a.isImportant) ? 1 : -1; });
    };
    PrioritizedTodosPipe = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["S" /* Pipe */])({
            name: 'prioritizedTodosPipe'
        }),
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["A" /* Injectable */])()
    ], PrioritizedTodosPipe);
    return PrioritizedTodosPipe;
}());

//# sourceMappingURL=prioritized-todos.js.map

/***/ }),

/***/ 287:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DoneTodosPipe; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

var DoneTodosPipe = (function () {
    function DoneTodosPipe() {
    }
    DoneTodosPipe.prototype.transform = function (todos) {
        return todos.filter(function (todo) { return todo.isDone; });
    };
    DoneTodosPipe = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["S" /* Pipe */])({
            name: 'doneTodosPipe'
        }),
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["A" /* Injectable */])()
    ], DoneTodosPipe);
    return DoneTodosPipe;
}());

//# sourceMappingURL=done-todos.js.map

/***/ })

},[205]);
//# sourceMappingURL=main.js.map