import { Pipe, PipeTransform, Injectable } from '@angular/core';
import { TodoModel } from '../shared/todo-model';

/**
 * Generated class for the PrioritizedTodosPipe pipe.
 *
 * See https://angular.io/api/core/Pipe for more info on Angular Pipes.
 */
@Pipe({
  name: 'prioritizedTodosPipe'
})
@Injectable()
export class PrioritizedTodosPipe implements PipeTransform {

  transform(todos: TodoModel[]) {
    return todos.filter( todo => !todo.isDone).sort((a, b) => (b.isImportant && !a.isImportant) ? 1 : -1);
  }
}
