import { Component } from '@angular/core';
import { NavController, NavParams, AlertController, LoadingController } from 'ionic-angular';
import { TodosPage } from '../todos/todos';
import { ListsServiceProvider} from '../../shared/lists-service';
import { ListModel } from '../../shared/list-model';

@Component({
  selector: 'page-lists',
  templateUrl: 'lists.html',
})
export class ListsPage {
  
  private selectedList: ListModel = null;

  constructor(public navCtrl: NavController,
     public navParams: NavParams,
     public alertCtrl: AlertController,
     private listsService: ListsServiceProvider,
     private loaderCtrl: LoadingController){
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ListsPage');
  }

  goToList(list: ListModel){
    this.clearSelectedList();
    this.navCtrl.push(TodosPage, {list});
  }

  addNewList(name: string){
    let loader = this.loaderCtrl.create();
    loader.present();
    this.listsService.addList(name)
      .subscribe(list => {
        this.goToList(list);
        loader.dismiss();
      }, error => {loader.dismiss();
    })
  }

  showAddList(){
    let addListAlert = this.alertCtrl.create({
      title: 'New List',
      message: 'Give a name to the new list',
      inputs: [
        {
          name: 'name',
          placeholder: 'Name'
        }
      ],
      buttons:[
        {
          text: 'Cancel',
          handler: data =>{}
        },
        {
          text:'Add',
          handler: data => { 
            let navTransition = addListAlert.dismiss();
            navTransition.then(()=>{this.addNewList(data.name)});
            return false;
           }
        }
      ]
    });

    addListAlert.present();
  }

  clearSelectedList(){
    this.selectedList = null;
  }

  selectList(list: ListModel){
    console.log('yes, i press and hold the tap');
    if(this.selectedList == list){
      this.clearSelectedList();
    } else {
      this.selectedList = list;
    }
  }

  removeSelectedList(){
    this.listsService.removeList(this.selectedList);
    this.selectedList = null;
  }

}
