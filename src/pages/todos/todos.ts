import { Component } from '@angular/core';
import { ModalController, Platform, NavParams, NavController } from 'ionic-angular';

import { TodoModel } from '../../shared/todo-model'
import { AddTaskModalPage } from '../add-task-modal/add-task-modal'
import { TodoServiceProvider } from '../../shared/todo-service';
import { ListModel } from '../../shared/list-model'

@Component({
  selector: 'page-todos',
  templateUrl: 'todos.html',
  providers: [TodoServiceProvider]
})
 
export class TodosPage {

  private toggleTodoTimeout = null;
  private list: ListModel;

  constructor(
    public navCtrl: NavController,
    public modalCtrl: ModalController,
    public todoService: TodoServiceProvider,
    public platform: Platform,
    public navParams: NavParams) {

      this.list = this.navParams.get('list');
      this.todoService.loadFromList(this.list.id);
    }

  ionViewDidLoad() {}

  ionViewWillUnload(){
    this.todoService.saveLocally(this.list.id);
  }

  setTodoStyles(item: TodoModel){
    let styles = {
      'text-decoration': item.isDone ? 'line-through' : 'none',
      'font-weight': item.isImportant ? '600' : 'normal' 
    };
    return styles;
  }

  toggleTodo(todo: TodoModel){
    if(this.toggleTodoTimeout){
      return;
    }
   this.toggleTodoTimeout = setTimeout(() => {
      this.todoService.toggleToDo(todo);
      this.toggleTodoTimeout = null;
    },
    this.platform.is('ios') ? 0 : 350 ); 
  }

  removeTodo(todo: TodoModel){
    this.todoService.removeTodo(todo);
  }

  showAddTodo(){
    let modal = this.modalCtrl.create(AddTaskModalPage,{listId: this.list.id} );
    modal.present();

    modal.onDidDismiss(data => {
      if(data){
        this.todoService.addTodo(data);
      }
    })
  }

  showEditTodo(todo: TodoModel){
    let modal = this.modalCtrl.create(AddTaskModalPage, {todo});
    modal.present();
    modal.onDidDismiss(data => {
      if(data){
        this.todoService.updateTodo(todo, data);
      }
    })
  }

}
