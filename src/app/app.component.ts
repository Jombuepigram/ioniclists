import { Component } from '@angular/core';
import { Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { ScreenOrientation } from '@ionic-native/screen-orientation';

import { ListsPage } from '../pages/lists/lists';


@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  rootPage:any = ListsPage;

  constructor(
    platform: Platform,
    statusBar: StatusBar,
    splashScreen: SplashScreen,
    screenOrientation: ScreenOrientation
    ) {

    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      statusBar.styleDefault();
      splashScreen.hide();

      if( screenOrientation.type == 'ios' || screenOrientation.type == 'android'){
        screenOrientation.lock(screenOrientation.ORIENTATIONS.PORTRAIT);
      }else{
        screenOrientation.unlock();
      }
    });
  }
}

